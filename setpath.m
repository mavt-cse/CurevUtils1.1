
% set the path to the CurveLab wrapping mex directory 
addpath('/Users/gerardot/Documents/Code/Tools/CurveLab-2.1.2/fdct_wrapping_cpp/mex')

% set the path to the utils directory
crvltutilsdir = '/Users/gerardot/Documents/MATLAB/Imaging/Curvelets/CurveletUtils';
addpath(crvltutilsdir)
addpath([crvltutilsdir '/utils'])
