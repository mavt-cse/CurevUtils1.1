/*
 *  CurveletUtils.h
 *
 *  Created by Tobias Gebäck on 12.9.2007.
 *  Copyright 2007 ETH Zurich. All rights reserved.
 *
 */

using namespace std;

#include "nummat.hpp"
#include "numvec.hpp"
//#include "numtns.hpp"
#include "fdct_wrapping_inc.hpp"

using namespace fdct_wrapping_ns;

namespace CurveletUtils {

typedef std::vector< std::vector<CpxNumMat> > CurveletData;
//typedef std::vector< std::vector<CpxNumTns> > CurveletData3D;

enum CurvUtilError { CUError_NoError = 0, CUError_BadParams = 1 };

void VarAndMean(const DblNumVec &vec, double &mean, double &var);

/* Mapping of indices and coordinates to closest index on other level */
void MapXYToLevel(const CurveletData &cdata, double x, double y, int tolev, int &ti, int &tj, int dir);
void MapToLevel(const CurveletData &cdata, int fromlev, int fromdir, int fi, int fj, int tolev, int &ti, int &tj, int dir);
void MapXYToGrid(int m, int n, double x, double y, int &ti, int &tj);
void MapToGrid(int m, int n, const CurveletData &cdata, int fromlev, int fromdir, int fi, int fj, int &ti, int &tj);
int GetQuadrant(const CurveletData &cdata, int level, int diridx);

/* Compute an all zero fdct, i.e. create the correct data structure */
CurvUtilError zero_fdct(int m, int n, CurveletData &cdata, int allcurvelets=0, int nscales=-1, int nangles_coarse=-1);

/* count nr of nonzero elements in CurveletData */
void countnnz(const CurveletData &cdata, int &nnz, int &nel);
CurvUtilError countnnz(const CurveletData &cdata, const IntNumVec &levels, int &nnz, int &nel);
int countnnz(const CurveletData &cdata);
CurvUtilError countnnz(const CurveletData &cdata, const IntNumVec &levels, int &nnz);

/* count nr of elements only */
int countnel(const CurveletData &cdata);
CurvUtilError countnel(const CurveletData &cdata, const IntNumVec &levels, int &nel);

/* Options passed to ExtractMultipleDirFields */
struct DirFieldOptions {
	int CurveletSize;  // The number of positions away that a curvelet coefficient influences
	int CircSumOffset; // running circular sum over -CSO..CSO
	int LocMaxGap;  // the required gap between maximums (for multiple field extraction)
};
	
/* Extract a directional field describing preferred direction at each grid point */
CurvUtilError ExtractMultipleDirFields(int m, int n, const CurveletData &cdata, const IntNumVec &levels, int nrfields, std::vector< std::vector<DblNumMat> > &dirfields, DirFieldOptions *opts = NULL);

/* Extract magnitude and direction (as index 0..7) at each grid point */
CurvUtilError ExtractDirAndMag(int m, int n, const CurveletData &cdata, const IntNumVec &levels, DblNumMat &mag, IntNumMat &dir);

/* Extract directional field, using only projections onto x- and y-axes */
CurvUtilError ExtractDirections(int m, int n, const CurveletData &cdata, const IntNumVec &levels, DblNumMat &xcmp, DblNumMat &ycmp);

/* Get magnitudes on specified levels, summed over all directions */
CurvUtilError GetMagnitude(int m, int n, const CurveletData &cdata, const IntNumVec &levels, DblNumMat &mag, int crvlt_size);


/* find Nth largest coefficient */
double FindNthCoeffSize(const CurveletData &c, int Ncoeffs);

/* threshold coefficients */
void ThresholdCoeffs(CurveletData &c, double thrsh);
CurvUtilError ThresholdCoeffs(CurveletData &c, double thrsh, const IntNumVec &levels);

void ChooseByCurvature(CurveletData &c, double T1, double T2, int lastklev);

}
